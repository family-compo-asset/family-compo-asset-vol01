本文档的中文版在[这里](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/README-CN.md).

# Family Compo Asset Library


[![License](https://img.shields.io/badge/license-CC0%20Public%20Domain-red)](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/LICENSE.md)

![Version](https://img.shields.io/badge/version-1.0-red)


# Why
Family Compo (F.COMPO) is a manga created by Hojo Tsukasa. I love this manga, and I'm eager to make mods of this manga (for non-commercial use only). Given my poor drawing skills, I believe that it is impossible to draw such beautiful characters in my mod. So, I'm trying to take the panels/sequences out of the original, then choose some panels to match the plot of my mod, and assemble them into my mod.


# How this asset library is designed
- In order to be able to choose the right panels to match my plot, I believe that I have to add tags to each panel. Then I can search the tags in this asset library to get related panels. For example, a panel with tags 'shion', 'joy' and 'livingroom' means a panel describing Shion has a joy expression in the living-room.

- When I search a tag in this asset library, many panels related to this tag will be listed. How can I find the best panel to match my plot? I think that I have to view the image content of each listed panel before I make the decision. 

- In order to make this asset library to be used as easily as possible, I decide to make it depends on as fewer soft-wares as possible. Finally, I decide to save each panel to a image file with related tags being its file name. So, I can search these image files in Explorer on Windows with tags being the keys, and efficiently browse the content of many listed images as thumbnails.

- This asset library is far from being perfect. Given my limited capability, this asset may contain inappropriate tags, and other flaws. But I hope this asset can be improved consistently to overcome these disadvantages. Based on this idea, firstly, I introduce special tags, such as ppl-x, act-x, exp-x, which will be explained in the following. Secondly, a file/panel in this asset may have flaw and should be replaced with a better one. Then, at least, I have to know to which page and which volume the panel belongs. So, I keep the volume and page information in the file name.

- In order to keep the file name as short as possible, I prefer singular form to plural form as possible as I can. For example, I tag 'tree' rather than 'trees' for a panel containing many trees. 

- Deciding which tag to be used is usually not an easy task. For example, a panel may show a scene containing many properties, such as coffee pot, cup, dishes. Should I add tags such as 'coffee_pot', 'cup' and 'dish' to the file name of this panel? or, should I use a single tag 'dinnerware' to represent them all? I'm not sure. For now, I temporarily use tag 'dinnerware' and 'prop-x' which aims to be refined or be improved in the future. For another example, given a panel showing the back of person, I can't decide who the person is without referring to the plot. In my opinion, it means this panel can be used to represent any other persons in my mod. So, I will add tag 'ppl-x' for this panel. Thus, I add special tags(e.g. ppl-x, prop-x, act-x, exp-x) which can be extended/refined later.

- Suppose that an event happens in the living-room in the original plot. But the panel has nothing related to living-room. In my opinion, tag 'livingroom' should not be added for this panel, because this panel can be used for other locations in my mod.


# How this asset library is created
Currently, the file_name is defined as the following format:

**file_name**

: index__[**people**]__scene__property__effect.jpg

[**people**] means **people__people__...__people** which means that the file/panel could contain several people.

And, **people** is defined as:
**people**
: name_direction_expression_action_dress_extension

I call each of the these tags a **tag_type**:
index, scene, property, effect, name, direction, expression, action, dress, extension

Each **tag_type** has optional values each of which is called a **tag_value**. A panel can have many tag_values of the same tag_type. These **tag_value**s can be connected with '_' out of order since the order generally doesn't matter. The available values for each **tag_type** are list below:

- NOTE:
    - a **tag_value** with prefix '+' means that this panel **HAS NOT** this **tag_value**(e.g. an expression, a person) **IN THE STORY OF F.COMPO**, but this panel **CAN BE USED FOR** this **tag_value**.  
    - a **tag_value** with prefix '-' means that this panel **HAS** this **tag_value**(e.g. an expression, a person) **IN THE STORY OF F.COMPO**, but this panel **DOES NOT SHOW** this **tag_value**.  

## index
**index** shows that the file belongs to which panel of which page of which volume. It has the following format:
**index**
: volume_page_panel

#### Values & Annotation :
    volume    # The volume index of F.COMPO to which each panel belongs, such as
              # v01, v02, ..., v13, v14. Family Compo has 14 volumes.
    page      # The page number to which each panel belongs, such as 
              # 001, 002, ...
    panel     # The sequence index of the panel on its page. 
              # **NOTE**: I define the sequence starts from 0 rather than 1. 
              # For example, if a page contains 9 panels, the panel index of
              # the panels on that page ranges from 0 to 8.

#### Examples :
    v01_001_0    # The file corresponds to volume 01, page 001, panel 0


## scene
The tag_type **scene** describes where the story in the panel happens, such as Wakanae's living-room. Some cases are complicated. For example, panel v01_061_6 is a sunset, instead of introducing a new tag 'sunset', I tag it with 'day' and 'night'. For another example, panel v01_187_1 shows a club at night in the original plot, but the panel does not seems like a night scene, so I tag it with 'day' and 'night' to make it a candidate for a day scene in the mod.

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation :
    scn-x       # this scene can't be described, and let it be processed later
    inner       # inner house scene
    day         # the scene out of houses at day
    night       # the scene out of houses at night
    wakanae     # wakanae's house
    yanagiba    # yanagiba's house
    studio      # Sora's studio (on the 2nd floor of Wakanae's house)
    storeroom   # one of the rooms on the 3rd floor of Wakanae's house

#### Examples :
    inner_wakanae_diningroom    # 



## property
A scene may have many properties, such as desk, table, bed and etc. 'desk', 'table', 'bed' are tag_value for tag_type 'property', and they can be added together out of order, e.g. desk_bed_table. 

When I create this asset library, I often don't tag all the properties at the first time, because I'm not sure which tags are best for this panel. I usually tag some major properties and 'prop-x' which is used to be refined/extended in the future.

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation :
    prop-x      # For unknown property, or for property which will be tagged later.
    niche       # Where the photos of Masahiko's mother and father are located
    cushion     # cushion/pillow
    balustrade  # For balustrade, railing, handrail
    device      # For e-device, such as game box, shave, clock
    light       # Traffic light, light on desk, light in house
    tear        # For tears, also for someone being cryings
    passerby    # For passerby, and for people whose name is unknown (e.g.
                # v01_115_2, v01_118_1, v01_113_4, v01_199_0)
    torch       # electric torch
    ladder      # connecting Sora's studio and the storeroom
    well        # connecting Sora's studio and the storeroom

    
#### Examples :
    desk_table_bed  #



## effect
This **tag_type** marks 2D effect. I'm not sure whether this **tag_type** is necessary, and this **tag_type** would be removed in the future if it is proved to be unnecessary. 

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation :
    fx-x        # For the effect which I don't know how to describe
    focus       # For the effect that something is focused
    wind        # e.g. hair is blew by the wind
    fast        # For the effect that something is moving fast
    clash       # For 
    mosaic      #
    sunny       # Sun light
    dialogue    #
    text        #
    black       # black background

#### Examples :
    (null)


## name
Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).
#### Values & Annotation :
    ppl-x     # For people can't be identified in a panel. For example, 
              # if people's face is not shown, then I don't know who he/she 
              # is without referring to the plot. It also means that this panel 
              # can be used for other people.
    younger   # For example, younger Shion, younger Masahiko, younger Sora and
              # younger Yukari.
    shion     # Shion Wakanae
    sora      # Sora Wakanae
    yukari    # Yukari Wakanae (Tatsuhiko Wakanae)
    masahiko  # Masahiko Yanagiba
    spouse    # Somebody's spouse who is not given a name
    
#### Examples :
    shion_younger  # For Shion as a little girl
    kyoko-spouse   # Kyoko's spouse who is not given a name


## direction
There are six directions if we consider the human body as a cube, such as front, back, top, down(bottom), right side, left side. This tag_type represents which directions of this people are shown in a panel. I combine right side and left side into one **tag_value**: side. So, the available **tag_value**s for this **tag_type** are: front, back, top, down, side.

For example, in v01_157_2, Shion is lying on the bed. But if we think her body as a cube, then what we see is the top side and down side. So, for this panel, the direction of Shion is: front_down.

Sometimes, different parts of a person have different directions. For example, in panel v01_003_4, the direction of Masahiko's head is front_side_down, but that of his torso is side. For now, I simply combine these values: front_side_down

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation:
    top     # the top direction of a person is shown in this panel
    down    # the down direction of a person  is shown in this panel
    side    # the right/left side direction of a person is shown in this panel
    front   # the front direction of a person  is shown in this panel
    back    # the back direction  of a person is shown in this panel
    
#### Examples:
    front_side_down # the front, side and down directions of torso (and head) are shown.
    front_h-side    # the front direction of torso and the side of head are shown.


## expression
I refer to the human emotions studied in [1]. And the name of some emotions is changed for the following two reasons:

  - In my option, 'Horror' and 'Fear' are similar for this manga, so I discard 'Horror'. 
  - I prefer shorter word for each expression, so I change the word for some expressions in [1]. 
```
     admiration             --> admire
     adoration              --> adore
     aesthetic appreciation --> apprec
     amusement              --> amuse
     awkwardness            --> awkward
     boredom                --> bored
     calmness               --> calm
     confusion              --> confused
     craving                --> crave
     entrancement           --> entranced
     excitement             --> excited
     empathetic pain        --> pain
     sadness                --> sad
     sexual desire          --> sexual
```
**tag_value** 'exp-x' means the expression can't be identified in a panel. For example, people's face is not shown in that panel. However, it means that this panel could be used for other expressions. So, I add 'exp-x' for that panel. For another example, an expression may be hard to be identified, then I guess the expression and add the corresponding tag_value, then I also append 'exp-x' to mark this expression should be  improved in the future.

Usually, I can't decide what exactly the expression in a panel is, then I simply choose some related expressions and combine their together. For example, v01_156_0.

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation (the annotation for each expression comes from [1] and Longman English dictionary) :
    exp-x     # For an expression that can't be or hard to be identified.
    admire    # feeling impressed, pride, amazement
              # impress: to make someone feel admiration and respect.
              #   pride: respect.
              #  amazed: =astonished =extremely surprised.
    adore     # love, adoration, happiness
              # adoration = great love & admiration
    apprec    # awe, calmness, wonder
              #    awe: great respect & liking with a slight fear.
              #   calm: relax and quiet, not angry, not nervous, not upset.
              # wonder: a feeling of surprise for something very beautiful,
              #         =admiration, =awe.
    amuse     # amusement, laughter, humor
              # amusement: funny, pleasure & enjoyment.
              #  laughter: when people laugh.
              #     humor: the ability or tendency to think that things are funny.
    anger     # anger, angry disgust, boiling with anger
    anxiety   # anxiety, fear, nervousness
              # anxiety: the feeling of being worried because you think that
              #          something bad has happened or will happened, and you 
              #          feel that you have no control over the situation.
              #    fear: the feeling you get when you are afraid or worried that
              #          something bad is going to happen.
              # nervous: worried or a little frightened about something and 
              #          unable to relax.
    awe       # awe, amazement, feeling impressed
              #     awe: great respect & liking with a slight fear.
              #  amazed: =astonished =extremely surprised.
              # impress: to make someone feel admiration and respect.
    awkward   # awkwardness, amused embarrassment, embarrassment
              #     awkward: feeling embarrassed because you are in a situation
              #              in which it is difficult to behave naturally.
              # embarrassed: feeling uncomfortable or nervous and worrying about
              #              what people think of you.
    bored     # boredom, annoyance(slight anger),
              #     bored: tired and impatient because you do not think 
              #            something is interesting.
              # annoyance: slight anger or impatience.
    calm      # calmness, peacefulness, serenity
              #     calm: relax and quiet, not angry, not nervous, not upset.
              # peaceful: quiet in a pleasant and relaxing way.
              #   serene: very calm and peaceful.
    confused  # confusion, curiosity, interested confusion
    crave     # hunger, desire, satiation of hunger
              # craving: an extremely strong desire for something.
              #  hunger: a strong need or desire for something.
    disgust   # disgust, feeling grossed out, extreme disgust
              #     disgust: a strong feeling of dislike, annoyance,
              #              or disapproval.
              # grossed out: to make someone wish they had not seen or been
              #              told about something because it is so unpleasant.
    pain      # pain, empathetic pain, shock. (empathy goes beyond sympathy)
    entranced # interest, amazement, feeling intrigued
              #  entrance: to give all attention to something/someone 
              #            because of they are beautiful or interesting.
              #    amazed: =astonished =extremely surprised.
              # intrigued: very interested in something because it seems
              #            strange or mysterious.
    excited   # excitement, adrenaline rush, awe
    fear      # fear, feeling scared, extreme fear
    interest  # interest, amazement, feeling intrigued
    joy       # happiness, extreme happiness,  love
    nostalgia # nostalgia, boredom, reminiscence
    relief    # relief, deep relief, sense of narrow escape
    romance   # love, romantic love, romance
    sad       # sadness, extreme sadness, sympathy
    satisfy   # feeling impressed, satisfaction, awestruck surprise
              #      impress: to make someone feel admiration and respect
              # satisfaction: a feeling of happiness/pleasure because you
              #               have achieved something or got what you wanted.
              #    awestruck: feeling great respect for the importance, 
              #               difficulty, or seriousness of someone/something.
    sexual    # sexual arousal, feeling horny(sexual excited), sexual desire
    surprise  # surprise, shock, amazement

#### Examples:
    bored_awkward_anger  # for the expression in the panel v01_156_0



## action
- In my opinion, sometimes, some panels can't be described very well by the above expressions. So I add **tag_type** 'action' to describe the panel more precisely. The available values for **tag_type** 'action' are listed below.

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation:
    act-x           # For the action which I don't know how to describe, or,
                    # for the action which I will describe it later.
    ahem            # clear throat
    bath            # including shower
    bend-over       # e.g. to bend the body over on a desk
    drunk           # e.g. someone being drunk
    palm-face       # face-palming
    fall            # e.g. someone being surprised and falls on the ground
    sweat           # for the sweat on the skin
    v-hand          # victory hand
    rest            # e.g. the gesture making the arm relax 
    drive           # e.g. drive the car
    point           # point something or a direction with finger or hand
    hand-on-head    # put hand on the head
    pour            # e.g. pour water

#### Examples:
    sit_cross-leg   #


## dress
- Frankly speaking, I don't know what is the good way to tag the dress in F.COMPO, because there are so many different dresses. Moreover, when I assemble several panels into my mod, dresses in those panels are often different, so I have to modify those dresses in Photoshop to get a uniform dress for a single mod.

- Often, a person wears several clothes at the same time, e.g. Sora often wears a t-shirt and a shirt. For this case, I simply assembly the above values. For example, I tag 'upper-long-white-black' for Sora's white shirt and black t-shirt.

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation :
    drs-x     # this dress will be processed later
    upper     # for upper body cloth
    lower     # for lower body cloth
    long      # for long sleeve or trousers
    short     # for short sleeve or trousers
    white     # cloth color: white or light
    gray      # cloth color: gray
    black     # cloth color: black or dark
    pattern   # the dress has a texture pattern rather than a solid color
    plaid     # Masahiko's shirts with grid texture
    shoe      # including foot with shoe
    DKNY      # one of Masahiko's clothes
    AMFC      # one of Shion's clothes
    nude      # no cloth

#### Examples :
    upper-long-black-plaid_lower-long-gray    # For long sleeve black and plaid clothes on the 
                                              # the upper body, and long gray trousers on the 
                                              # lower body 
    lower-nude                                # lower body without cloth, or the cloth is not shown.


## extension
A part of the body may have a special action in a panel, so I collect them into this type of tag.

Some values and annotation are listed here. For all the values and annotations, please refer to [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### Values & Annotation :
    arm             # for a special arm action in a panel
    foot            # for a special foot action in a panel
    hand            # for a special hand action in a panel
    cute            # cute figure
    shoulder        # e.g. someone puts his/her hand on this person's shoulder
    silhouette      #
    
#### Examples :
    hand
    

# How to use it
1. Download the asset library. For now, the asset library for these volumes are finished: All Volumes.
    1. Access this page: [https://gitlab.com/family-compo-asset](https://gitlab.com/family-compo-asset). It lists the asset library for all volumes(volume 01 - 14). 
    2. Access an asset library for a volume. For example, for volume 01, the link is:[https://gitlab.com/family-compo-asset/family-compo-asset-vol01](https://gitlab.com/family-compo-asset/family-compo-asset-vol01)
    3. Find and click the download button. This will download asset library for volume 01.
    4. Download the asset library for other volumes if they are finished. 
2. Unzip the downloaded file to a directory.
3. Search the tag_value(e.g. shion surprise, masahiko awkward) as keys.


# FAQ
- Q: File name with so many tags will exceed 255 characters, which is not allowed in Windows and Linux. How to deal with it?
- A: Suppose file name index_tags_string.jpg exceeds 255 characters. My solution is:
    1. cloning the image. Now there will be two identical images, say it index_0.jpg and index_1.jpg
    2. cut string tags_string into two parts: tags_string_part0, tags_string_part1
    3. assign index_tags_string_part0.jpg to index_0.jpg, and assign index_tags_string_part1.jpg to index_1.jpg. For example: panel v01_022_0 , panel v01_168_0.

- Q: Which the Family Compo resource is used in this asset?
- A: The panels are taken out of the original F.COMPO (Japanese version, image resolution: 1600x1120. Thank you for sharing this high-resolution FC!).

- Q: How to contact the owner of this project?
- A: You can leave a message [here](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/issues).


# References
[1] Alan S. Cowen and Dacher Keltner (2017), Self-report captures 27 distinct categories of emotion bridged by continuous gradients, https://doi.org/10.1073/pnas.1702247114 


# License
This work (the name and tags of the files) is licensed under the terms of the [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/).

![](https://licensebuttons.net/p/zero/1.0/80x15.png)

To the extent possible under law, the person who associated CC0 with this work has waived all copyright and related or neighboring rights to this work.


# Fair Use Policy and Legal Disclaimer
This project contains the images clipped from the manga Family Compo (F.COMPO) the use of which is not specifically authorized by the copyright owner. I am making this project available to the fans community in honor of Hojo Tsukasa. In accord with my non-profit purpose and the following works based on this project, I really hope these efforts can bring me a 'fair use' of containing the copyrighted images of Family Compo (F.COMPO) in this project. If you wish to use the copyrighted images of Family Compo (F.COMPO) for purposes of your own, you must obtain permission from the copyright owner.

  - Based on this project, I am creating my mods about the manga Family Compo (F.COMPO). [Here is that project](https://gitlab.com/family-compo-asset/family-compo-mods).
  - Based on this project, I am writing reviews and analysis & statistics about the manga Family Compo (F.COMPO). [Here is that project](https://gitlab.com/city4cat/myblogs/tree/master/hojocn/).

If containing the copyrighted images of Family Compo (F.COMPO) in this project would not be allowed, please let me know by leaving a message [here](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/issues). 
