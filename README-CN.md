The English version of this document is [here](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/README.md).

# 非常家庭素材库(Family Compo Asset Library)


[![License](https://img.shields.io/badge/license-CC0%20Public%20Domain-red)](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/LICENSE.md)

![Version](https://img.shields.io/badge/version-1.0-red)


# 为什么做这个库
非常家庭 (F.COMPO) (以下简称FC)是北条司的一部漫画. 我很喜欢这部漫画，很想制作漫画MOD(只作非商用). 由于个人的绘画水平很糟糕，很难画出FC里那么精美的人物. 所以我尝试把漫画里的分镜(分格/panel/sequence)取出来, 然后搭配剧情, 最后组织成MOD.


# 一些想法
- 为了能让分格更好地搭配MOD的剧情, 我觉得需要给每个分格打上标签(tag)。 然后，可以在素材库里搜索这些tag，以得到相关的panels. 例如，用'shion'（紫苑）, 'joy'（开心）, 'livingroom'（客厅）这些tag来表示‘紫苑在客厅很开心’ 这样一个panel。

- 当在素材库里搜索某个tag时，会列出很多搜索结果. 如何找到最适合MOD剧情的那个panel呢？我觉得必须要看panel的图片内容才能作决定. 

- 为了能让素材库尽可能地使用方便, 我觉得素材库应该尽可能少地依赖软件工具. 所以决定把每一个panel保存为一个图片文件，其文件名是相关的tag. 这样，可以在Windows的资源管理器里以tag为关键字去搜索这些图片, 并且能以缩略图的形式快速浏览所列出的这些图片.

- 这个素材库远不够完善。由于个人能力有限，tag可能选取的不合适，甚至存在错误。 但我希望素材库能不断地更新和改进. 基于这样的想法，首先，引入了一些特殊的tag，比如, ppl-x, act-x, exp-x. 这些tag在下面有说明。其次，后续可能会发现素材库里文件(panel)有缺陷，需要更换。那么，这至少需要知道该文件来源于第几卷的第几页。因此，在文件名里保留了卷信息和页码。

- 为了使文件名尽可能地短, tag倾向于使用名词单数形式而不是复数形式. 例如, 使用'tree'而不是'trees'来标记一个有很多树的panel. 

- 很多时候不太容易确定使用什么tag. 例如，一个scene(场景)里有很多property(道具)： 咖啡壶，茶杯，碟子。 是否需要添加 'coffee_pot', 'cup' and 'dish' 这些tag? 或者，只使用一个tag 'dinnerware'(餐具)来表示它们？ 现在不确定哪种方法更好. 目前为止, 暂时使用tag 'dinnerware'和'prop-x'。'prop-x'表示这个panel可以后续再细化或者改进. 再例如, 假设一个panel显示的是一个人物的背面, 如果不看原著剧情的话，很可能知道这个人物是谁. 但这也意味着该panel在MOD里可用来表示其他人物. 所以，考虑为这个panel加上tag 'ppl-x'. 基于这些考虑，在素材库里引入了一些特殊的tags(ppl-x, prop-x, act-x, exp-x)，以供后续扩展和细化.


# 素材库制作的一些细节
目前，文件名的格式如下:

file_name

: index__[**people**]__scene__property__effect.jpg

[**people**] 表示 **people__people__...__people**, 这表示文件(panel)里可以有多个人物。

其中**people**的格式如下：
people 
: name_direction_expression_action_dress_extension

下面这些tag称为**tag_type**(tag类型):
index, scene, property, effect, name, direction, expression, action, dress, extension

每个**tag_type**会有若干可选的值，我称之为**tag_value**（标签值）. 一个panel可能含有同一个**tag_type**里的多个**tag_value**。这些**tag_value**可以用‘_’连接，一般来说，不必讲究顺序。以下详细说明**tag_type**和其**tag_value**:

- 注意：  
    - 以'+'为前缀的**tag_value**表示**在F.COMPO剧情里**该panel没有体现出该**tag_value**(例如，没有某个表情、某个人)，但该panel**可以被用来表示**该**tag_value**.  
    - 以'-'为前缀的**tag_value**表示**在F.COMPO剧情里**该panel有该**tag_value**(例如，有某个表情、某个人)，但该panel**没有体现出**该**tag_value**.  

## index（序号）
**index**的格式如下:
index
: volume_page_panel

它表示文件对应于第几卷的第几页的第几个panel.

#### 值 & 注解 :
    volume    # 卷。取值： v01, v02, ..., v13, v14。v表示volume。FC共14卷。
    page      # 页。取值： 001, 002, ...
    panel     # 分格。在该页的第几个分格。从0开始计数。例如, 若某页有9个分格，则该页的panel计数为从0到8

#### 例:
    v01_001_0  # 01卷001页0分格


## scene（场景）
**scene**表示panel里的故事发生的地点, 比如，若苗家的客厅. 有些情况会有些复杂. 例如, 分格v01_061_6是一个日落的场景，没有用tag 'sunset', 而是用 'day'和 'night'. 再例如，分格v01_187_1 ，虽然在原著里表示的是夜晚的夜总会，但panel里看不出是夜晚的场景。 所以，使用tag'day'和 'night'，便于后续用于白天或夜晚的情况.

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    scn-x       # 单从图片里无法确定该场景，或留作以后完善。
    inner       # 室内场景
    day         # 室外场景，白天
    night       # 室外场景，夜晚
    wakanae     # 若苗家  
    yanagiba    # 柳叶家 
    club        # 夜总会，卡拉OK歌厅
    hotel       # 旅馆
    park        # 游乐场， 公园
    porch       # 门口
    balcony     # 阳台
    bathroom    # 浴室，卫生间
    diningroom  # 餐厅
    bedroom     # 卧室
    kitchen     # 厨房
    livingroom  # 客厅
    classroom   # 教室
    school      # 学校，包括大学
    studio      # 空的漫画工作室（若苗家2楼）
    storeroom   # 若苗家三楼的一个屋子

#### 例:
    inner_wakanae_diningroom  # 若苗家，客厅


## property（道具）
scene可能有很多道具，如：desk书桌, table餐桌, bed床，等等. 'desk', 'table', 'bed' 是**tag_type**'property'下的**tag_value**, 它们可以用字符‘_’相连，不讲究顺序, 例如. desk_bed_table. 
    
 制作素材库时，我通常没有一次性地列出scene里显示的所有property, 因为我不确定使用哪些**tag_value**最合适. 一般我会标记那些主要的property，然后加上**tag_value** 'prop-x'，表示之后会扩展或细化。

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    prop-x          # 针对不知道名字的property, 或，针对有待以后确定、细化、扩展的property。
    niche           # 佛龛（放雅彦父母照片的地方）, 
    cushion         # 厚垫子/枕头, 
    balustrade      # 楼梯栏杆/围栏栏杆, 
    device          # 电子设备, 例如， 游戏机，表，剃须刀
    light           # 路灯/室内灯/书桌灯, 
    tear            # 泪（也表示哭）, 
    toiletry        # 卫生间用品,     
    passerby        # 路人,  或没有名字的人 (例如：v01_115_2, v01_118_1,
                    # v01_113_4, v01_199_0)
    torch           # 手电筒
    ladder          # 梯子
    well            # 天井（连接空的工作室和储藏室）
    
#### 例:
    desk_table_bed  #


## effect（特效）
表示2D的特效。我不确定这个**tag_type**是否有必要。如果之后发现没有必要的话再删掉吧。

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    focus       # 聚焦的效果
    wind        # 例如头发被风吹拂的效果
    fast        # 快速移动的效果
    clash       # 表示惊讶，或碰撞
    mosaic      # 马赛克
    sunny       # 阳光明媚的效果
    dialogue    # 对话框
    text        # 文字
    black       # 黑色背景

#### 例:
    (无)
    

## name（人物名）

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    ppl-x     # 不能确定是哪个人物。例如，人物的脸被遮挡，这也意味着在MOD里这个图可以被充当别人。
    younger   # 表示年幼 或 年轻。比如，年幼的紫苑、雅彦，年轻时的空、紫。
    shion     # Wakanae若苗 Shion紫苑 
    sora      # Wakanae若苗 Sora空 
    yukari    # Wakanae若苗 Yukari紫  (Wakanae若苗 Tatsuhiko龙彦)
    masahiko  # Yanagiba柳叶 Masahiko雅彦 
    spouse    # 某人没有给出名字（但他是另一个人的配偶）
    
#### 例:
    shion_younger  # 年幼的紫苑
    shion_cap      # 能否用这个tag_value表示男生装扮的紫苑？
    kyoko-spouse   # 京子的丈夫(没有给出姓名)


## direction（人物的朝向）
如果把人体想象成一个立方体盒子，盒子有6个面：front前, back后, top顶, down(bottom)底, right side右侧, left side左侧。tag_type direction表示哪些面显示出来了。left side和right side合并为一个值:side. 所以， tag_value为：front, back, top, down, side.
    
例如，v01_157_2, 紫苑躺在床上。如果把她的身体想象成一个盒子的话，我们看到的是盒子的前面和底面。所以，对于v01_157_2的紫苑，direction的值为front_down.

有时候，人物身体的不同部位显示的面不同。例如，v01_003_4, 雅彦的头部为front_side_down, 他的躯干为side. 目前，我简单地把它们合并起来作为最终的值：front_side_down

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    top     # 顶部被显示出来了
    down    # 底部被显示出来了
    side    # 侧面被显示出来了
    front   # 前面被显示出来了
    back    # 后面被显示出来了
    
#### 例:
    front_side_down  


## expression（人物表情）
我参考了[1]里面提到的人的情绪，基于此描述表情. 基于以下两个原因，修改了[1]里的情绪的名称:

  - 在我看来，漫画里的'Horror恐惧'和'Fear害怕'两个表情很类似,所以我只用‘Fear’；
  - 希望使用尽可能短的英文单词。

```
         admiration             --> admire
         adoration              --> adore
         aesthetic appreciation --> apprec
         amusement              --> amuse
         awkwardness            --> awkward
         boredom                --> bored
         calmness               --> calm
         confusion              --> confused
         craving                --> crave
         entrancement           --> entranced
         excitement             --> excited
         empathic pain          --> pain
         sadness                --> sad
         sexual desire          --> sexual
```
'exp-x' 表示暂时无法确定该表情. 比如该panel看不到人脸. 但这也意味着该panel可用于其他表情. 所以标记为'exp-x'. 再比如，一个表情不太容易确定，我猜测它可能被算作是哪些表情，然后再附加上‘exp-x’以供后续处理。

我经常发现很难决定一个panel里的人物表情，然后就把与之相关的表情集合起来作为最后的结果。例如，v01_156_0.

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 : (关于表情的注解，来自于[1]):
    exp-x     # 针对表情未知，或，表情不容易确定的情况
    admire    # 崇拜。feeling impressed令人崇拜的、尊敬的, pride尊敬, amazement震惊
              # impress: to make someone feel admiration and respect令人崇拜的、尊敬的。
              #   pride: respect尊敬。
              #  amazed: =astonished震惊 =extremely surprised极其吃惊。
    adore     # 喜爱。love爱, adoration崇拜, happiness开心
              # adoration = great love & admiration非常喜爱、崇拜
    apprec    # 对美的欣赏。awe敬畏, calmness平静, wonder惊喜
              #    awe: great respect&liking with a slight fear（非常尊敬， 附带些许害怕）
              #   calm: 放松，平静，不生气，不紧张，不沮丧
              # wonder: a feeling of surprise for something very beautiful
              #        （惊讶于某物的美）, =admiration, =awe。
    amuse     # 逗乐，好笑。amusement逗乐/好笑(pleasure愉快 & enjoyment享受/欣赏),
              # laughter大笑, humor幽默
              # amusement: funny, pleasure & enjoyment（可笑的，开心，愉快）
              #  laughter: when people laugh（大笑）
              #     humor: the ability or tendency to think that things are funny
    anger     # 怒。anger怒, angry disgust厌恶, boiling with anger无法抑制的愤怒
    anxiety   # 紧张。anxiety紧张, fear害怕, nervousness紧张
              # anxiety: 因预感不好的事情将发生，且无法控制它，而导致的紧张
              # nervous: 因担心/害怕而导致的紧张
    awe       # 敬畏。awe敬畏, amazement非常吃惊, feeling impressed令人尊敬的/崇拜的
              #     awe: great respect&liking with a slight fear
              #        （非常尊敬， 带有些许害怕）
              #  amazed: =astonished震惊 =extremely surprised极其吃惊
              # impress: to make someone feel admiration and respect令人崇拜的、尊敬的。
    awkward   # 尴尬。awkwardness尴尬, amused embarrassment逗乐的尴尬, 
              # embarrassment困境，难堪
              #     awkward: 行动不自在而感到尴尬
              # embarrassed: 感到不自在、紧张，担心别人对自己的看法
    bored     # 厌倦。boredom(not interested), annoyance(slight anger),
              #     bored: 因不感兴趣而厌倦、失去耐心
              # annoyance: 有点生气，没有耐心
    calm      # calmness(平静), peacefulness(安静）, serenity（非常地平静、安静）
              #     calm: 放松，平静，不生气，不紧张，不沮丧
              # peaceful:（愉快、放松地）安静
              #   serene: 非常地平静、安静
    confused  # confusion困惑, curiosity好奇, interested confusion感兴趣，又有些困惑
    crave     # 极其强烈的欲望。hunger（强烈的欲望）, desire（欲望）, 
              # satiation of hunger（满足欲望）
    disgust   # 厌恶
    pain      # 痛苦。pain, empathic pain共情的痛苦, shock震惊 (共情比同情更强烈)
    entranced # 被美丽或有趣所吸引。 interest, amazement极其惊讶, 
              # feeling intrigued（非常有兴趣）
              #  entrance: 被美丽或有趣所吸引
              #    amazed: =astonished震惊 =extremely surprised极其吃惊
              # intrigued: 因某事物的不寻常或神秘，而对其非常有兴趣
    excited   # 兴奋。excitement, adrenaline rush令人兴奋到肾上腺飙升, awe敬畏
    fear      # 害怕。fear, feeling scared, extreme fear极其害怕
    interest  # 感兴趣。interest, amazement非常惊讶（震惊）, feeling intrigued（非常有兴趣）
    joy       # 高兴。happiness（快乐）, extreme happiness（极其快乐）,  love
    nostalgia # 怀旧。nostalgia, boredom厌倦, reminiscence（回忆过去的美好）
    relief    # 放轻松。relief, deep relief, sense of narrow escape（解脱）
    romance   # 浪漫。love, romantic love, romance
    sad       # 悲伤。sadness, extreme sadness极度悲伤, sympathy同情
    satisfy   # 满足。feeling impressed(令人尊敬的/崇拜的), satisfaction成就感, 
              # awestruck惊叹 surprise
              #      impress: to make someone feel admiration and respect
              #               令人崇拜的、尊敬的
              # satisfaction: 因做成某事，或得到想要的，而开心快乐
              #    awestruck: 因某事物的重要、困难、严肃，而感到非常尊敬。
    sexual    # 色色的。sexual arousal性兴奋, feeling horny(性兴奋), sexual desire性欲
    surprise  # 惊讶。surprise, shock震惊, amazement非常惊讶
    
#### 例:
    sad_anxiety_awkward  #


## action（人物动作）
我觉得有时候只用表情很难描述一个panel的内容，所以增加了动作标签.

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    act-x       # 不确定该如何描述这个动作，或者,留作以后改进。
    ahem        # 清嗓子，引起他人注意
    bath        # 洗澡, 包括淋浴
    bend-over   # 例如，趴在桌子上
    drunk       # 例如，喝醉
    palm-face   # 扶额
    fall        # （因惊讶或尴尬而）倒地
    sweat       # 汗
    v-hand      # V字手型pose
    rest        # 例如：让胳膊放松的姿势
    drive       # 开车
    point       # 用手或手指指某物或某方向
    hand-on-head    # 手放在头上
    pour            # 倒（水）
    
    
#### 例:
    sit_cross-leg


## dress（人物的衣服）
坦白地说，目前没有找到好的方法给FC里服饰打标签，因为里面有太多的服饰. 同时，当在MOD里组织panel时，不同panel里的服装很可能是不一致的，所以需要在Photoshop去修改这些服饰.

一个panel里，人物会穿很多件衣服。比如，里面穿衬衣，外面穿外套。对于这些情况，我简单地罗列下列tag_value. 比如，我用'upper-long-white-black' 表示某人所穿的白色外套和黑色衬衣。
    
这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    drs-x     # 该服饰留作以后处理
    upper     # 上半身的衣服
    lower     # 下半身的衣服
    long      # 长袖或长裤
    short     # 短袖或短裤
    white     # 白色或浅色
    gray      # 灰色
    black     # 黑色或深色
    pattern   # 服饰的图案：杂色
    plaid     # 服饰的图案：格子：例如，雅彦的格子衫
    shoe      # 鞋(包括穿鞋的脚)
    nude      # 没穿衣服

#### 例:
    upper-long-black-plaid_lower-long-gray    # 上衣长袖黑色+格子， 下身长裤灰色
    lower-nude                                # 下半身没有(显示)衣服


## extension（扩展项）
表示身体的某个部位会有特殊的动作。

这里只列出部分值和解释. 完整的信息请参考 [./fc_tag.py](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/fc_tag.py).

#### 值 & 注解 :
    arm   # 胳膊有特殊的动作
    foot  # 脚有特殊的动作
    hand  # 手有特殊动作
    cute  # Q版
    shoulder        # 别人把手放在这个人肩上
    silhouette      # 轮廓
    
#### 例:
    hand


# 如何使用该素材库
1. 下载该素材库. 目前完成的卷有：全部
    1. 访问: [https://gitlab.com/family-compo-asset](https://gitlab.com/family-compo-asset). 它列出了01-14卷对应的素材库链接。
    2. 点击其中某卷. 例如, 01卷的链接是: [https://gitlab.com/family-compo-asset/family-compo-asset-vol01](https://gitlab.com/family-compo-asset/family-compo-asset-vol01)
    3. 找到下载按钮，并点击. 这会下载01卷的素材库。
    4. 类似的操作下载其他卷的素材库。
2. 把下载的文件解压缩至某个目录。
3. 在该目录下搜索tag_value（例如，shion surprise, masahiko awkward）。


# 常见问题
- Q: 这么多tag_value会导致文件名的长度超过 255个字符, 这是操作系统不允许的。怎么办？
- A: 假设文件名index_tags_string.jpg 超过了255个字符。我的办法是：
    1. 把文件拷贝一份. 这样就有两个内容完全相同的文件，简称为 index_0.jpg 和 index_1.jpg
    2. 把过长的文件名字符串tags_string 分割为两部分: tags_string_part0, tags_string_part1
    3. 把index_0.jpg的文件名设置为index_tags_string_part0.jpg， 把index_1.jpg的文件名设置为index_tags_string_part1.jpg. 可以参考panel v01_022_0, panel v01_168_0.

- Q: 该素材库里的图片来源于哪里？
- A: 来源于F.COMPO原著（网上日文版，图片分辨率大约为: 1600x1120。感谢您分享了这个高清的FC资源！）。

- Q: 如何联系该项目的所有者？
- A: 可以在[这里](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/issues)留言.


# 参考资料
1. Alan S. Cowen and Dacher Keltner (2017), Self-report captures 27 distinct categories of emotion bridged by continuous gradients, https://doi.org/10.1073/pnas.1702247114 


# 使用许可
本作品(文件的名字和tag)采用[CC0 1.0 通用 (CC0 1.0) 公共领域贡献](https://creativecommons.org/publicdomain/zero/1.0/deed.zh)进行许可。

![](https://licensebuttons.net/p/zero/1.0/80x15.png)

在法律允许的范围内，将CC0与该作品相关联的人放弃了对该作品的所有版权以及相关或邻近的权利。


# 合理使用和免责声明
该项目包含了漫画《非常家庭》(F.COMPO)的图片，其使用未得到版权方的明确授权。我把该项目贡献给漫迷是为了向北条司大师致敬。考虑到我的非营利意图和基于该项目所作的其他工作，我真心希望这些努力能让我‘合理地’在本项目里包含《非常家庭》(F.COMPO)的图片。如果你希望使用《非常家庭》(F.COMPO)的图片，请务必获得版权方的许可。

  - 基于本项目, 我正在制作《非常家庭》(F.COMPO)的MOD。[项目在这里](https://gitlab.com/family-compo-asset/family-compo-mods)。
  - 基于本项目, 我正在写《非常家庭》(F.COMPO)评论，以及分析和统计。[项目在这里](https://gitlab.com/city4cat/myblogs/tree/master/hojocn/)。

如果在本项目里包含《非常家庭》(F.COMPO)的图片是不允许的, 请在[这里](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/issues)留言告诉我。
