
![](https://licensebuttons.net/p/zero/1.0/80x15.png)

This work is licensed under the terms of the [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/).

To the extent possible under law, the person who associated CC0 with this work has waived all copyright and related or neighboring rights to this work.

本作品采用[CC0 1.0 通用 (CC0 1.0) 公共领域贡献](https://creativecommons.org/publicdomain/zero/1.0/deed.zh)进行许可。

在法律允许的范围内，将CC0与该作品相关联的人放弃了对该作品的所有版权以及相关或邻近的权利。


