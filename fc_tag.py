
# ------------------------------------------------------------------------------
# NAME
# ------------------------------------------------------------------------------
name = {
'ppl-x':0,    # For people can't be identified in a square. For example, if people's face is not shown, then I don't know who he/she is without referring to the plot. It also means that this square can be used for other people.
              # 不能确定是哪个人物。例如，人物的脸被遮挡，这也意味着在MOD里这个图可以被充当别人。
'younger':0,  # e.g. shion_younger (Shion as a little girl)
              # 表示年幼 或 年轻。比如，shion_younger表示年幼的紫苑
              
              
'shion':0,    # Shion Wakanae
              # Wakanae若苗 Shion紫苑 
'shya' :0,    # Shionoya, Dressing as a boy, Shion calls herself Shionoya in front of Yoko(Vol11, page 011)
              # 塩谷Shionoya, 男装时，在叶子面前紫苑自称塩谷（11卷011页）
'masahiko':0, # Masahiko Yanagiba
              # Yanagiba柳叶 Masahiko雅彦 
'masami':0,   # Masahiko dressing as a girl
              # 雅美
              
              
'sora':0,     # Sora Wakanae（before the marriage：Kikuchi Haruka ）
              # Wakanae若苗 Sora空 （婚前： 菊地Kikuchi 春香Haruka ）
'yukari':0,   # Yukari Wakanae (her name before the marriage: Tatsuhiko Wakanae，vol2 page171)
              # Wakanae若苗 Yukari紫  (婚前： Wakanae若苗 Tatsuhiko龙彦，02卷171页)


'mori':0,     # Mori, the female editor who pushes Sora to complete the work before the deadline.
              # 森（督促空工作的女编辑）

'hiromi':0,   # Sora's assistant: Hiromi Nakamura (real name: Nakamura中村 Mitsuhiro光浩)
              # 空的助手： Nakamura中村 Hiromi浩美  (本名: Nakamura中村 Mitsuhiro光浩)
'makoto':0,   # Sora's assistant: Makoto Yamazaki
              # 空的助手： Yamazaki山崎 Makoto真琴 
'susumu':0,   # Sora's assistant: Yokota Susumu 
              # 空的助手： Yokota横田 Susumu进              
'kazuko':0,   # Sora's assistant: Kazuko Niigata (real name: Kazuto Niigata)
              # 空的助手： Niigata新潟 Kazuko和子 (本名: Niigata新潟 Kazuto和人)
'kazuko-boy':0, # kazuko dressing as a man
                # Niigata新潟 Kazuko和子 男装
'kazuma':0,   # Niigata Kazuko's child, (vol08 ch54)
              # Niigata新潟 Kazuko和子的孩子：一马（08卷54话）


'grandma':0,  # Shion's Granma
              # 紫苑的奶奶
'grandpa':0,  # Shion's Granpa
              # 紫苑的爷爷
'mama':0,     # Masahiko's mother
              # 雅彦的妈妈
'papa':0,     # Masahiko's father
              # 雅彦的爸爸
       
              
'yoriko':0,   # Sora's little sister: Yoriko Kikuchi
              # 空的妹妹：Yoriko顺子 Kikuchi菊地
'kenji':0,    # Sora's schoolmate 
              # 奥村 宪司Kenji。空的高中同学
'shoko':0,    # kenji's daughter（vol04 page07）
              # 奥村 章子Shoko。宪司Kenji的女儿（04卷07页）
              
              
'yoko':0,     # Yoko(Youko) Asaoka
              # Asaoka浅冈 Yoko(Youko)叶子
'ykma':0,     # Yoko's mama
              # 叶子的妈妈 


'kaoru':0,    # Kaoru Masoba
              # Masoba真朱 Kaoru薰
'saki':0,     # Kaoru's mother: Saki 
              # 薰的妈妈：Saki早纪               
'tatsumi':0,  # Kaoru's father: Tatsumi Masoba
              # 薰的爸爸：Masoba真朱 Tatsumi辰四 
'aoi':0,      # The person who likes Tatsumi
              # (美)葵。喜欢辰巳的人


'ejima':0,    # Masahiko's classmate: Takuya Ejima（vol05 page025）
              # 雅彦的同学：Takuya卓也 Ejima江岛（05卷025页）
'ejima-girl':0, # Ejima dressing as a girl
                # 女装的江岛卓也 
'akane':0,    # Fujisaki Akane, the girl in the film club who likes Masami（vol06 ch41）
              # 藤崎Fujisaki 茜Akane，影研社里喜欢雅美的那个女孩（06卷41话）
'nishina':0,  # Nishina Kohei, one of Masahiko's classmates who likes Masahiko（vol10 page148，vol14 page004）
              # Kohei耕平 Nishina仁科，雅彦的同班同学，喜欢雅彦（10卷148页，14卷004页）
'chinatsu':0, # Chinatsu(tall with light hair), who is introduced by Shion to Film Research Society（vol12 page141，vol12 page158）
              # 千夏Chinatsu(个子高，发色浅)。和紫苑一起加入映研社（12卷141页，12卷158页）
'mai':0,      # Mai(short with dark hair). who is introduced by Shion to Film Research Society（vol12 page141，vol12 page158）
              # 麻衣Mai(个子矮，发色深)。和紫苑一起加入映研社（12卷141页，12卷158页）
'asagi':0,    # Asagi Ai, a schoolmate of Shion, who has been loving Shion from elementary school to unversity（vol14 ch93）
              # Asagi浅葱 Ai蓝，紫苑的同学，从小学到大学一直喜欢紫苑（14卷93话）
                
'director':0, # the film director in the film club
              # 影研社的电影导演
'cameraman':0,# the cinematographer in the film club
              # 影研社的摄像师              

'reiko':0,    # Saito Reiko (Shion is her first love, in Vol1 CH6)
              # 齐藤Saito 玲子Reiko (紫苑是她的初恋，见第1卷第6话)
'matsu':0,    # Matsushita Toshifumi, a baseball palyer who loves Shion（vol9 ch59）
              # Matsushita松下 Toshifumi敏史, 棒球运动员, 他喜欢紫苑（9卷59话）

  
'risa':0,     # Risa(tall with light hair), a schoolmate of Shion, who takes a graduation trip with Shion (Vol11 ch76)
              # 理沙Risa(个子高，发色浅)，紫苑的高中同学，和紫苑一起高中毕业旅行（11卷76话）
'mika':0,     # Mika(short with dark hair), a schoolmate of Shion, who takes a graduation trip with Shion (Vol11 ch76)
              # 美菜Mika(个子矮，发色深)，紫苑的高中同学，和紫苑一起高中毕业旅行（11卷76话）

'fumiya':0,   # Saitou Fumiya, Mika's boyfriend, wearing a black shirt and gray trousers（vol12 page010）
              # Saitou齐藤 Fumiya文哉，美菜的男友, 穿黑上衣、灰裤子（12卷010页）
'kazuki':0,   # Kazuki, Fumiya's schoolmate, wearing a glasses and short pants（vol12 page012）
              # Kazuki一树，Fumiya文哉 的同学, 戴一幅眼镜、短裤（12卷012页）
'tooru':0,    # Tooru, Fumiya's schoolmate, wearing light shirt and black trousers（vol12 page012）
              # Tooru阿透，Fumiya文哉的同学，穿浅色上衣、黑裤子（12卷012页）
'te-chan':0,  # Te, another boyfriend of Mika（vol12 page55）
              # 哲，美菜的另一个男友（12卷55页）

'kimihiro':0, # Furuya Kimihiro, the man dating with Hiromi online（vol09 ch58）
              # Furuya古屋 Kimihiro公弘, 和浩美网恋的人（09卷58话）
'tomomi':0,   # Furuya Tomomi, Furuya Kimihiro's little sister（vol09 ch58）
              # Furuya古屋 Tomomi朋美, Furuya古屋 Kimihiro公弘的妹妹（09卷58话）
              
'kyoko':0,    # Yukari's classmate in the flower arrangment club: Kyoko Hanzu (Before marriage: Kyoko Isaka)（vol02 ch14）
              # 紫的中学插花社的成员：Hanzu八不 Kyoko京子 (婚前：Isaka井阪 Kyoko京子)（02卷14话）
'aya':0,      # Yukari's classmate in the flower arrangment club: Aya（vol02 ch14）
              # 紫的中学插花社的成员：Aya绫子（02卷14话）
'tenko':0,    # Yukari's classmate in the flower arrangment club: Tenko（vol02 ch14）
              # 紫的中学插花社的成员：Tenko典子（02卷14话）
'kenji-ma':0, # Kenji's mother

'hanzu':0,    # Mr.Hanzu, Kyoko's husband who is not given a name
              # 八不先生，Kyoko京子的丈夫
'teacher':0,  # teacher, the chairman of the university, 
              # 老师，学校校长
'villain':0,  # 坏人
'nurse':0,    # 护士
'club-boss':0,#
'boss':0,     # 
'police':0,   # 警察
'player':0,   # player in a band or a sport team
              # 乐队或体育运动队里的乐手
'sth':0,      # something

'chairman':0, # the chairman of Masahiko's university,  (to be replaced with 'teacher')
              # 雅彦大学的校长, 老师              
'ryo':0,      # Saeba Ryo (to be removed)
              # 獠
'boy':0,      # A girl is dressing as a boy (to be removed)
'girl':0,     # A boy is dressing as a girl (to be removed)
'old-woman':0,# (to be removed)
'old-man':0,  # (to be removed)
'masahiko-girl' :0,  # Masahiko dressing as a girl
                     # 雅美
#'shion-boy' :0,     # Shion dressing as a boy
#                    # 男装的紫苑
}# end of name
# ------------------------------------------------------------------------------
# DIRECTION
# ------------------------------------------------------------------------------
direction = {
'top':0,      # the top direction of torso is shown
              # 顶部被显示出来了
'down':0,     # the down direction of torso  is shown
              # 底部被显示出来了
'side':0,     # the right/left side direction of torso is shown
              # 侧面被显示出来了
'front':0,    # the front direction of torso  is shown
              # 前面被显示出来了
'back':0,     # the back direction  of torso is shown
              # 后面被显示出来了
              
# the following marks the direction of head when it is different from the direction of torso
'h-top':0,    # the top direction of head is shown, and the top direction of torso is not shown
'h-down':0,   # the down direction of head is shown, and the down direction of torso is not shown
'h-side':0,   # the side direction of head is shown, and the side direction of torso is not shown
'h-front':0,  # the front direction of head is shown, and the front direction of torso is not shown
'h-back':0,   # the back direction of head is shown, and the back direction of torso is not shown
# For example, 
# 'front_side_down' means the front, side and down directions of torso (and head) are shown.
# 'front_h-side' means the front direction of torso and the side of head are shown.
}# end of direction
# ------------------------------------------------------------------------------
# EXPRESSION
# ------------------------------------------------------------------------------
expression = {
'exp-x':0,    # For an expression that can't be or hard to be identified.
              # 针对表情未知，或，不容易确定的表情
'admire':0,   # 崇拜。feeling impressed(令人崇拜的、尊敬的), pride(尊敬), amazement(震惊)
              # impress: to make someone feel admiration and respect(令人崇拜的、尊敬的)。
              #   pride: respect(尊敬)。
              #  amazed: =astonished(震惊) =extremely surprised(极其吃惊)。
              
'adore':0,    # 喜爱。love(爱), adoration(崇拜), happiness(开心)
              # adoration = great love & admiration(非常喜爱、崇拜)
              
'apprec':0,   # aesthetic appreciation(对美的欣赏)
              # awe(敬畏), calmness(平静), wonder(惊喜)
              #    awe: great respect&liking with a slight fear（非常尊敬， 附带些许害怕）
              #   calm: 放松，平静，不生气，不紧张，不沮丧
              # wonder: a feeling of surprise for something very beautiful
              #        （惊讶于某物的美）, =admiration, =awe。
              
'amuse':0,    # 逗乐，好笑。amusement(逗乐/好笑)(pleasure(愉快) & enjoyment(享受/欣赏)),
              # laughter(大笑), humor(幽默)
              # amusement: funny, pleasure & enjoyment（可笑的，开心，愉快）
              #  laughter: when people laugh（大笑）
              #     humor: the ability or tendency to think that things are funny
              
'anger':0,    # 怒。anger(怒), angry disgust(厌恶), boiling with anger(无法抑制的愤怒)

'anxiety':0,  # 紧张。anxiety(紧张), fear(害怕), nervousness(紧张)
              # anxiety: the feeling of being worried because you think that
              #          something bad has happened or will happened, and you 
              #          feel that you have no control over the situation.
              #          (因预感不好的事情将发生，且无法控制它，而导致的紧张)
              #    fear: the feeling you get when you are afraid or worried that
              #          something bad is going to happen.
              # nervous: worried or a little frightened about something and 
              #          unable to relax.
              #          (因担心/害怕而导致的紧张)
              
'awe':0,      # 敬畏。awe(敬畏), amazement(非常吃惊), feeling impressed(令人尊敬的/崇拜的)
              #     awe: great respect&liking with a slight fear
              #        （非常尊敬， 带有些许害怕）
              #  amazed: =astonished(震惊) =extremely surprised(极其吃惊)
              # impress: to make someone feel admiration and respect(令人崇拜的、尊敬的)
              
'awkward':0,  # 尴尬。awkwardness(尴尬), amused embarrassment(逗乐的尴尬), 
              #     awkward: feeling embarrassed because you are in a situation
              #              in which it is difficult to behave naturally.
              #              (行动不自在而感到尴尬)
              # embarrassed: feeling uncomfortable or nervous and worrying about
              #              what people think of you.
              #              (感到不自在、紧张，担心别人对自己的看法)
              
'bored':0,    # 厌倦。boredom(not interested), annoyance(slight anger),
              #     bored: tired and impatient because you do not think 
              #            something is interesting.
              #            (因不感兴趣而厌倦、失去耐心)
              # annoyance: slight anger or impatience.
              #            (有点生气，没有耐心)
              
'calm':0,     # calmness(平静), peacefulness(安静）, serenity（非常地平静、安静）
              #     calm: relax and quiet, not angry, not nervous, not upset.
              #           (放松，平静，不生气，不紧张，不沮丧)
              # peaceful: quiet in a pleasant and relaxing way.
              #          （（愉快、放松地）安静）
              #   serene: very calm and peaceful.
              #           (非常地平静、安静)
              
'confused':0, # confusion(困惑), curiosity(好奇), interested confusion(感兴趣，又有些困惑)

'crave':0,    # 极其强烈的欲望。hunger（强烈的欲望）, desire（欲望）, satiation of hunger（满足欲望）
              # craving: an extremely strong desire for something.
              #          (极其强烈的欲望)。
              #  hunger: a strong need or desire for something.
              
'disgust':0,  # 厌恶。disgust, feeling grossed out, extreme disgust
              #     disgust: a strong feeling of dislike, annoyance,
              #              or disapproval.
              # grossed out: to make someone wish they had not seen or been
              #              told about something because it is so unpleasant.

'pain':0,     # 痛苦。pain, empathetic pain(共情的痛苦), shock(震惊)(empathy goes beyond sympathy共情比同情更强烈)

'entranced':0,# 被美丽或有趣所吸引。 interest, amazement(极其惊讶), feeling intrigued（非常有兴趣）
              #  entrance: to give all attention to something/someone 
              #            because of they are beautiful or interesting.
              #            (被美丽或有趣所吸引)
              #    amazed: =astonished(震惊) =extremely surprised(极其吃惊)
              # intrigued: very interested in something because it seems
              #            strange or mysterious.
              #            (因某事物的不寻常或神秘，而对其非常有兴趣)

'excited':0,  # 兴奋。excitement, adrenaline rush(令人兴奋到肾上腺飙升), awe(敬畏)

'fear':0,     # 害怕。fear, feeling scared, extreme fear(极其害怕)

'horror':0,   # 恐惧。shock(震惊), horror(恐惧), feeling scared(感到害怕)

'interest':0, # 感兴趣。interest, amazement(非常惊讶（震惊）), feeling intrigued（非常有兴趣）

'joy':0,      # 高兴。happiness（快乐）, extreme happiness（极其快乐）,  love

'nostalgia':0,# 怀旧。nostalgia, boredom(厌倦), reminiscence（回忆过去的美好）

'relief':0,   # 放轻松。relief, deep relief, sense of narrow escape（解脱）

'romance':0,  # 浪漫。love, romantic love, romance

'sad':0,      # 悲伤。sadness, extreme sadness(极度悲伤), sympathy(同情)

'satisfy':0,  # 满足。feeling impressed(令人尊敬的/崇拜的), satisfaction(成就感), awestruck(惊叹) surprise
              #      impress: to make someone feel admiration and respect
              #               (令人崇拜的、尊敬的)
              # satisfaction: a feeling of happiness/pleasure because you
              #               have achieved something or got what you wanted.
              #               (因做成某事，或得到想要的，而开心快乐)
              #    awestruck: feeling great respect for the importance, 
              #               difficulty, or seriousness of someone/something.
              #               (因某事物的重要、困难、严肃，而感到非常尊敬)。

'sexual':0,   # 色色的。sexual arousal(性兴奋), feeling horny(性兴奋), sexual desire(性欲)

'surprise':0, # 惊讶(意想不到)。surprise, shock(震惊), amazement(非常惊讶)

}# end of expression
# ------------------------------------------------------------------------------
# Action
# ------------------------------------------------------------------------------
action = {
'act-x':0,    # For the action which I don't know how to describe, or, for the action which I will describe it later.
              # 不确定该如何描述这个动作，或者,留作以后改进。
'ahem':0,     # clear throat. caugh
              # 清嗓子，引起他人注意. 咳嗽
'shh':0,      # 嘘（示意他人保持安静）
'bath':0,     # including shower
              # 洗澡, 包括淋浴
'bend-over':0,# e.g. to bend the body over on a desk
              # 例如，趴在桌子上
'bow':0,      # 低头
#'stoop':0     # 俯身，弯腰 
'carry':0,    # carry somebody (on the back)
              # 背在身上（肩上）
'chin-rest':0, # 托腮
'knee-rest':0, # arm rests on the knee
               # 胳膊放在膝盖上休息
'arm-rest':0,  # arm rests on the something
               # 胳膊放在某处休息
'leg-rest':0, # arm rests on the leg/foot
               # 胳膊放在腿/脚上休息
'arm-up':0,    # lift arm
               # 抬起胳膊
'chore':0,    # do the chores
              # 做家务
'clap':0,     # 鼓掌
'close-eye':0,# 闭眼
'open-eye':0, # 睁开眼(wake? to be obsoleted? )
'half-eye':0, # half close eye, half open eye
              # 半闭眼, 半睁眼
'comfort':0,  # 安慰
'cook':0,     # 做饭
'cross-arm':0,# 胳膊交叉
'cross-leg':0,# 双腿交叉（或翘二郎腿)
'drink':0,    # 喝
'drunk':0,    # e.g. someone being drunk
              # 例如，喝醉
'tired':0,    # tired, yawn
              # 累, 打哈欠
'sleepy':0,   # 瞌睡
'palm-face':0,# face-palming
              # 扶额
'fall':0,     # e.g. someone being surprised and falls on the ground
              # （因惊讶或尴尬而）倒地
'trip':0,     # 绊倒(should be replaced with 'fall'?)
'fight':0,    # 打架
'hit':0,      # 砸
'hungry':0,   # 饿
'kiss':0,     # 吻
'kneel':0,    # 跪
'lie':0,      # with face upward or downward
              # 躺(或趴)
'open':0,     # open something
              # 打开...
'close':0,    # close something
              # 关闭...
'open-door':0,  # 开门
'close-door':0, # 关门
'open-window':0, # 开窗
'close-window':0,# 关窗
'open-fridge':0, # 开冰箱
'close-fridge':0,# 关冰箱
'peek':0,     # 偷看，斜眼看
'play':0,     # play game, play sports
              # 玩游戏，打球
'push':0,     # push, turn on/off the switch
              # 推, 按开关

'quarrel':0,  # 争吵
'argue':0,    # 争论
'run':0,      # 跑
'search':0,   # 寻找
'shiver':0,   # 发抖
'sick':0,     # sick, catch a code, etc...
              # 不舒服，生病，呕吐, 感冒，...
'sigh':0,     # 叹气
'breath':0,   # 呼吸，呼气，吸气
'sing':0,     # 唱歌
'sit':0,      # 坐
'sleep':0, # 睡觉
'sneeze':0,   # 打喷嚏
#'splash':0,   # 溅水花
'squat':0,    # 蹲
'sweat':0,    # for the sweat on the skin
              # 汗
'throw':0,    # 扔
'tilt':0,     # 倾斜，斜靠
'turn':0,     # turn head or body
              # 转头，转身
'wave':0,     # 挥手
'whisper':0,  # 悄悄话
'yell':0,     # 大声喊
'wink':0,     # 挤眼睛
'v-hand':0,   # victory hand, OK hand, other hand gestures
              # V字手型pose, ok 手势, 其他手势
'hand-on-face':0, # put hand on the face
                  # 手放在脸颊上
'hand-on-head':0, # put hand on the head
                  # 手放在头上
'hand-on-hip':0,  # put hand(s) on the hip(s), or, put hand(s) into one's pocket(s)
                  # 手放在腰部（比如，手掐腰，或手揣兜里）
'hand-on-mouth':0,# put hand on the mouth/chin
                  # 手放在嘴/下巴上
'hand-on-breast':0, # 
                    # 手放在胸前
'cross-hand':0,   # cross the palms
                  # 双手交叉
'cross-finger':0, # cross the fingers
                  # 双手指交叉
'hand-behind':0,  # put the hand behind
                  # 把手背在后面

#'glue':0,        # glue something (use 'work' instead)
                  # 用胶水粘(用work替代)
'break':0,    # e.g. break the cup, broken clothes
              # 例如，摔（碎）杯子, 衣服破损
'jump':0,     # 跳
'rest':0,     # e.g. the gesture making the arm relax 
              # 例如：让胳膊放松的姿势
'drive':0,    # e.g. drive the car
              # 开车
'point':0,    # point something or a direction with finger or hand
              # 用手或手指指某物或某方向

'hug':0,      # 拥抱
'pour':0,     # e.g. pour water
              # 倒（水）
'sneak':0,    # 悄悄地，偷偷地
'knock':0,    # 敲
'knock-door':0, # 敲门
'suspect':0,  # 怀疑
'contempt':0, # 蔑视 (disgust?)
'hold':0,     # e.g. hold a cup of tea
              # 例如，端茶杯
'drag':0,     # 拖，拽            
'pull':0,     # 拉(to be replaced with 'drag'?)  
'hang':0,     # 拎起来
'fist':0,     # hold hand as a fist
              # 握拳
'hold-fist':0,# hold hand as a fist
              # 握拳
'hold-breast':0,# 
'shy':0,      # 害羞
'ride':0,     # ride bike
              # 骑自行车
'stand':0,    # 站立 
'stand-up':0, # stand up, sit down
              # 站起来，坐下
'walk':0,     # 走路
'work':0,     # e.g. Sora is drawing, Masahiko/Shion is studying, yukari is serving
              # 工作（例如，空的画漫画，雅彦/紫苑的学习, 端盘子）
'repair':0,   # repair something
              # 修理东西
'eat':0,      # have a dinner
              # 吃饭
'wash':0,     # 洗东西
'clean':0,    # 清洁（房间）
'look-up':0,  # look upward
              # 向上看
'look-down':0,# look downward
              # 向下看
'wake':0,     # 醒来
'accost':0,   # 搭讪
'profess':0,  # 表白
'nightmare':0,# 做噩梦
'beg':0,      # 恳求
'pray':0,     # 祈祷(to be replaced with 'beg'?)
'plead':0,    # 恳求(to be replaced with 'beg'?)
'blow':0,     # 吹气
'cheer':0,    # 庆祝，欢呼
'shake':0,    # 握手
'take-off':0, # 脱衣服
'put-on':0,   # 穿衣服
'stretch':0,  # stretch oneself, yawn
              # 伸懒腰, 打哈欠
'smoking':0,  # 抽烟
'dance':0,    # 跳舞
'refuse':0,   # 拒绝
'vomit':0,    # 吐
'spread-leg':0,   # spread leg
                  # 张开(腿)
'head-up':0,  # 抬头
'disinterest':0,  # 冷漠
'black-face':0,   # face with black lines
                  # 脸上有黑线
'cold':0,     # 寒 
'snicker':0,  # 坏笑
'pout':0,     # (not show tooth)
              # 噘嘴(不露齿)
'bare-teeth':0, # (show tooth) (should be replaced with 'open-mouth')
              # 呲牙（露齿）
'faint':0,    # 昏倒
'hurt':0,     # 受伤
#'injury':0,   # 伤痕(to be replaced with 'hurt'?)
'hide':0,     # 躲藏
'spy':0,      # 偷看
'resolute':0, # 坚定的 (to be replaced with serious?)
'determine':0,# 坚毅 (to be replaced with serious?)
'apologize':0,  # 道歉
'hold-door':0,  # hold the door 
                # 扶门
'fishing':0,    # 钓鱼
'touch':0,      # 抚摸 
'thumb-up':0,   # 赞
'pull-ear':0,   # 揪耳朵
'soak':0,       # 浸泡
'tongue':0,     # stick tongue out
                #
'lift-foot':0,   # 抬脚
'sprawl':0,      # 伸展（四肢）
'confident':0,   # 有信心的
'struggle':0,    # 挣扎
'welcome':0,     # welcome or goodbye, open door then introduce somebody
                 # 欢迎或再见, 开门然后介绍某人
'whistle':0,     # 吹口哨
'seized':0,      # 被捕获
'snap':0,        # 打响指
'read':0,        # 
'crouch':0,      # 蜷缩
'snug':0,        # 依偎
'tiptoe':0,      # 踮脚
'envy':0,        # 妒忌
'wipe':0,        # wipe tear. 擦（眼泪）
'rip':0,         # 撕
'pick':0,        # 捡起，拾起，抠鼻孔
'shrug':0,       # shrug, disperse hand
                 # 耸肩，摊手
'climb':0,       # 爬
'salute':0,      # 敬礼
'scratch':0,     # 挠
'sniff':0,       # 嗅，闻
'kidnap':0,      # kidnap, squeeze
                 # 绑架, 挤压
'squeeze':0,     # e.g. squeeze somebody with the arm
                 # 挤压, 例如，用胳膊挤压
'elbow':0,       # an elbow action whose name I don't know
                 # 一个胳膊肘的动作（我不知道这个动作的名称）
'get-up':0,      # 起床
'move-back':0,   # 后退
'blush':0,       # 脸红 
'tap':0,         # 轻拍, touch?(11_102_0)
'flirt':0,       # 搔首弄姿
'dizzy':0,       # 眩晕
'drop':0,        # 放下
'throttle':0,    # 勒死，掐死，使窒息
'hand-on-neck':0, # 勒, 手放在脖子附近
'crane':0,       # crane, or stretch neck upward
                 # 探头看，或向上伸脖子
'deliver':0,     # 递（东西）
'make-face':0,   # 做鬼脸
'upside-down':0, # 倒立，倒挂
'support':0,     # support with arm or hand
                 # 用手支撑
'seduce':0,      # 诱惑
'harass':0,      # (sexual) harassment
                 # (性)骚扰
'latch':0,       # to latch on to something with mouth
                 # 用嘴叼住某物
'eye-shadow':0,  # it has the shadow/glasses around the eyes, so you can't see his/her eyes
                 # 有眼部的阴影/镜片，导致看不到眼睛
'make-up':0,     # 化妆
}# end of action
# ------------------------------------------------------------------------------
# DRESS
# ------------------------------------------------------------------------------
dress = {
'drs-x':0,    # this dress will be processed later
              # 不确定如何描述该服装，或留作以后细化。
'upper':0,    # for upper body cloth
              # 上半身的衣服
'lower':0,    # for lower body cloth
              # 下半身的衣服
'long':0,     # for long sleeve or trousers
              # 长袖或长裤
'short':0,    # for short sleeve or trousers
              # 短袖或短裤
'white':0,    # cloth color: white or light
              # 白色或浅色
'gray':0,     # cloth color: gray
              # 灰色
'black':0,    # cloth color: black or dark
              # 黑色或深色
'pattern':0,  # the dress has a texture pattern rather than a solid color
              # 服饰的图案：杂色
'striped':0,  # striped pattern
              # 条带状图案
'plaid':0,    # Masahiko's shirts with grid texture
              # 服饰的图案：格子：例如，雅彦的格子衫
'apron':0,    # 围裙
'skirt':0,    # 裙子
'bathrobe':0, # 浴巾
'belt':0,     # 腰带
'glove':0,    # 手套
'jeans':0,    # 牛仔裤
'shoe':0,     # including foot with shoe
              # 鞋(包括穿鞋的脚)
'slipper':0,  # 拖鞋
'sneaker':0,  # 运动鞋
'tie':0,      # 领带/领结
'tights':0,   # 紧身裤
'underwear':0,# 内衣
'bra':0,      # 胸罩(be replaced with 'underwear'?)
'DKNY':0,     # one of Masahiko's clothes
              # 雅彦的一个衣服
'AMFC':0,     # one of Shion's clothes
              # 紫苑的一个衣服
'nude':0,     # no cloth
              # 没穿衣服
'kimono':0,   # a traditional Japanese dress
              # 和服 
'CK':0,       # one of Masahiko's clothes
              # 雅彦的一个衣服
'BOY':0,      # one of Shion's clothes
              # 紫苑的一个衣服
'SPRIT':0,    # one of Shion's clothes
              # 紫苑的一个衣服         
'uniform':0,  # 校服
'bikini':0,   # 泳装
'canvas':0,   # 画板
'plumb':0,    # 铅锤
'towel':0,          # 毛巾/浴巾, 
}# end of dress
# ------------------------------------------------------------------------------
# EXTENSION
# ------------------------------------------------------------------------------
extension = {
'arm':0,      # for a special arm action in a square
              # 胳膊有特殊的动作
'foot':0,     # for a special foot action in a square
              # 脚有特殊的动作
'hand':0,     # for a special hand action in a square
              # 手有特殊动作
'cute':0,     # cute figure
              # Q版
'shoulder':0, # e.g. someone puts his/her hand on this person's shoulder
              # 别人把手放在这个人肩上
'silhouette':0, # 轮廓
'head':0,     # 头
'eye':0,      # 眼睛
'breast':0,   # 胸部
'leg':0,      # 腿部
'hair':0,     # 头发
'mouth':0,     # for a special mouth action in a square
'neck':0,      # 脖子
'butt':0,      # 屁股  
'coin':0,      # 硬币
'whitehair':0, # hair is not shaded black
               # 头发未上色
}# end of extension
# ------------------------------------------------------------------------------
# SCENE
# ------------------------------------------------------------------------------
scene = {
'scn-x':0,      # this scene can't be described, and let it be processed later
                # 单从图片里无法确定该场景，或留作以后完善。
'inner':0,      # inner house
                # 室内场景
'day':0,        # out of houses at day
                # 室外场景，白天
'night':0,      #  out of houses at night
                # 室外场景，夜晚
'wakanae':0,    # Wakanae's house
                # 若苗家 
'wakanae-old':0,# the house where 
                # 若苗家 
'yanagiba':0,   # Yanagiba's house
                # 柳叶家
'yoko-house':0,    # 叶子住处
'ejima-house':0,   # 江岛住处
'masoba-house':0,  # Tatsumi辰巳 Masoba真朱, Kaoru薰 Masoba真朱的家住处
'tatsu-office':0,  # Tatsumi's office
                    # 辰巳的办公室
'office':0,        # 办公室
'grandpa-house':0, # to be replaced with hotel ?
'kenji-house':0,   # 
'kazuko-house':0,  #
'kimihiro-house':0, # the house of Furuya Kimihiro and Furuya Tomomi
                    # Furuya古屋 Kimihiro公弘和Furuya古屋 Tomomi朋美的住处

'porch':0,      # e.g. the porch of Wakanae's house
                # 门口， 例如，若苗家的门口
'balcony':0,    # Wakanae's balcony
                # 阳台
'roof':0,       # 屋顶

'bathroom':0,   # 浴室，卫生间
'diningroom':0, # 餐厅
'bedroom':0,    # 卧室
'kitchen':0,    # kitchen next to diningroom, or kitchen(sink?) in the studio
                # 餐厅旁的厨房，或工作室里的厨房(洗漱池)
'livingroom':0, # 客厅
'livingroom2':0, # 连接工作室和走廊的那个屋子
'niche-room': 0, # one of the rooms on the 1st floor of Wakanae's house, it is the place where niche is located
                 # 和室(若苗家1楼的一个屋子, 摆放雅彦父母照片的屋子)
'studio': 0,    # Sora's studio (on the 2nd floor of Wakanae's house)
                # 空的漫画工作室（若苗家2楼）
'guestroom':0,  # Wakanae's house has a guest room, Kaoru's bedroom
                # 若苗家的客房(熏的卧室)
'storeroom': 0, # one of the rooms on the 3rd floor of Wakanae's house. another place in 1st floor
                # 若苗家三楼的一个屋子, 一楼的一个地方
'yard':0,       # Wakanae's house has a yard
                # （若苗家的）后院  

'vicinity':0,   # the vicinity of X, e.g. wakanae_vicinity
                # 某物附近，例如，wakanae_vicinity(若苗家附近)
                
'club':0,       # night club or Karaoke club
                # 夜总会，卡拉OK歌厅
'hotel':0,      # hotel, the hotel of yoko family
                # 旅馆, 叶子家的旅馆
'park':0,       # 游乐场， 公园
'resort':0,     # tourist attraction
                # 旅游景点
'shop':0,       # 商店 

'classroom':0,  # 教室
'school':0,     # including the university
                # 学校，包括大学

'station':0,    # 车站
'hall':0,       # 学校（开学典礼）大厅
                
'film-club':0,  # the film club that Masahiko joined
                # 雅彦加入的影研社

'hospital': 0,     # 医院
'beach': 0,        # 海滩
'holiday': 0,      # 节日
'street': 0,       # 街
'backstreet': 0,   # 后街
'changing-room': 0,   # a place for changing cloth
                      # 换衣间
'church': 0,    # 教堂  

's-passage': 0, # passage as a scene
's-car': 0,     # car as a scene, e.g. in the car/train
's-road':0,     # road as a scene
'ground':0,     # ground with grass
's-landscape':0,# landscape as a scene
'party':0,      # throw a party, 开派对
}# end of scene
# ------------------------------------------------------------------------------
# PROPERTY
# ------------------------------------------------------------------------------
prop = {
'prop-x':0,         # For unknown property, or for property which will be tagged later.
                    # 针对不知道名字的property, 或，针对有待以后确定、细化、扩展的property。
'furniture':0,      # 家具
'air-conditioner':0, # 空调, 
'cloth':0,          # 衣服
'desk':0,           # 书桌, 
'niche':0,          # Where the photos of Masahiko's mother and father are located
                    # 佛龛（放雅彦父母照片的地方）, 
'bookshelf':0,      # 书架
'fan':0,            # 风扇
'faucet':0,         # 水龙头 
'frame':0,          # the frame of a picture
                    # 照片/画的框
'chair':0,          # 椅子
'curtain':0,        # 窗帘,
'cushion':0,        # cushion/pillow
                    # 垫子/枕头
'mat':0,            # 薄垫子
'sofa':0,           # 沙发, 
'stairs':0,         # 楼梯,
'switch':0,         # 灯的开关,
'poster':0,         # 海报
'blind':0,          # 百叶窗
'balustrade':0,     # For balustrade, railing, handrail
                    # 楼梯栏杆/围栏栏杆,
'floor':0,          # 地板
'ceiling':0,        # 天花板
'device':0,         # For e-device, such as game box, shave, clock, copycat, ticket checking machine, ticket gate, heater,
                    # 电子设备, 例如， 游戏机，表，剃须刀, 复印机, 电脑, 检票机, 闸机, 热水器, 
'instrument':0,     # 乐器
'computer':0,       # 电脑
'fridge':0,         # 冰箱
'cleaner':0,        # 吸尘器
'clock':0,          # 表
'heater':0,         # 热水器
'washer':0,         # 洗衣机
'mic':0,            # 麦克风
'bag':0,            # 书包
'basket':0,         # 篮子
'umbrella':0,       # 伞
'bandage':0,        # bandage, the towel for cooling a fever, mask
                    # 绷带, 发烧降温用的毛巾, 口罩
'dinnerware':0,     # 餐具
'chopstick':0,      # 筷子
'candy':0,          # 糖
'food':0,           # 食物
'stationery':0,     # 文具
'hankie':0,         # hankie is used when Sora the manga, or handkerchief.
                    # 空画漫画时需要用的手帕
'ink':0,            # 墨水
'letter':0,         # 信
'light':0,          # Traffic light, light on desk, light in house
                    # 路灯/室内灯/书桌灯
'paper':0,          # paper or card
                    # 纸, 卡片
'pen':0,            # 笔
'photo':0,          # 相片
'newspaper':0,      # 报纸
'book':0,           # 书
'album':0,          # 相册 (to be replaced with 'book'?)
'garbage':0,        # 垃圾/杂物
'smoke':0,          # smoke, vapor
                    # 烟/蒸汽
'tear':0,           # For tears, also for someone being cryings
                    # 泪（也表示哭）
#'toilet':0,         # 马桶，卫生间(to be replaced with 'bathroom')

'mirror':0,         # 镜子
'shave':0,          # 剃须刀, 
'door':0,           # 门,
'doorplate':0,      # 门牌, 
'plate':0,          # plate,
                    # 板,
'bell':0,           # 门铃,
'passage':0,        # 走廊, 
'passerby':0,       # For passerby, and for people whose name is unknown (e.g. v01_115_2, v01_118_1, v01_113_4, v01_199_0)
                    # 路人,  或没有名字的人 (例如：v01_115_2, v01_118_1,
'waiter':0,         # waiter in the club
                    # 酒吧/夜总会的侍者
'pole':0,           # utility pole
                    # 电线杆
'wall':0,           # 墙,
'window':0,         # 窗
'blind-road':0,     # 盲人道
'bridge':0,         # 桥,
'bucket':0,         # 桶
'building':0,       # 建筑物
#'building-house':0,       # 住宅
#'building-mansion':0,     # 大廈
'drainage':0,       # 下水道,
'road':0,           # 马路
'fountain':0,       # 喷泉
#'ground':0,         # 地面
'landscape':0,      # landscape, sculpture(statue), cave
                    # 风景 , 雕塑(雕像)，溶洞/岩洞
'tree':0,           # tree, Chrismas tree
                    # 树, 圣诞树
'mountain':0,       # 山, 
'moon':0,           # 月亮,
'car':0,            # 车, 
'train':0,          # train, ropeway/cable-car, bus. (should be replaced with 'car'?)
                    # 列车, 索道/缆车, 公交车
'bike':0,           # 自行车, 
'roller-coaster':0, # 过山车, 
'ferris-wheel':0,   # 摩天轮, 
'spaceship':0,      # 飞船
'toy':0,            # toy, e.g. trumpet
                    # 玩具, 例如：喇叭
'dragonfly':0,      # 蜻蜓, 
'dust':0,           # 尘土, (to be replaced with 'smoke'?)
'plant':0,          # 植物
'sky':0,            # 天空
'cloud':0,          # 云
'star':0,           # 星星 
'pin':0,            # 图钉
'glue':0,           # 胶水
'saliva':0,   # 口水
'leaf':0,     # 树叶
'plane':0,    # 飞机
'map':0,      # 地图
'torch':0,    # electric torch
              # 手电筒
'ladder':0,   # connecting Sora's studio and the storeroom
              # 梯子
'well':0,     # connecting Sora's studio and the storeroom
              # 天井（连接空的工作室和储藏室）
'tv':0,       # 电视
'shadow':0,   # 阴影
'cup':0,      # cup, bottle, drinks, beverage, ...
              # 杯子, 瓶子, 喝的，饮料, ...
'bottle':0,   # 瓶子 (to be replaced with 'cup')
'disk':0,     # 碟子
'table':0,    # 餐桌 
'flower':0,   # 花 
'water':0,    # 水
'splash':0,   # 溅水花
'box':0,      # e.g. box, hussif
              # 例如： 盒子, 针线盒
'bed':0,      # 床(to be replaced with 'bedlinen')
'bedlinen':0, # 床上用品
'hat':0,      # cap, hat, Christmas hat
              # 帽子, 圣诞帽
'phone':0,    # 电话
'booth':0,    # 电话亭
'sock':0,     # 袜子
'ring':0,     # the ring of the phone
              # (电话)铃声
'fence':0,    # 围栏

'handle':0,   # the handle of a door
              # （门）把手
'pipe':0,     # pipe on the wall
              # 墙上的管道
'bird':0,     # 鸟   
'stone':0,    # 石头  
'grass':0,    # 草  (ground)
'wardrobe':0, # 衣柜
'sink':0,     # the sink in the bathroom or the kitchen
              # 洗手间或厨房的水池
'toiletry':0, # 卫生间用品, 化妆用品
'eardrop':0,  # 耳环(to be replaced with 'toiletry')
#'rouge':0,    # 口红(to be replaced with 'toiletry')
'condom':0,   # 避孕套
'camera':0,   # camera or projector
              # 摄像机，放映机
'gun':0,      # 枪
'bullet':0,   # 子弹
'blood':0,    # 血迹              
'film':0,     # 电影, 电影胶卷，录像带
'screen':0,   # film screen
              # 荧幕   
'glass':0,    # 玻璃
'glasses':0,  # 眼镜
'gift':0,     # 礼物
'wallet':0,   # 钱包
'oven':0,     # 烤箱
'pot':0,      # 锅
'spoon':0,    # 勺子
'bowl':0,     # 碗
'toothbrush':0,  # 牙刷
'ball':0,  # baseball， football
           # 篮球，足球

'stick':0, # stick, bat
           # 棍子， 球棒
'tomb':0,  # 墓
'sea':0,   # 海
'lift':0,  # 电梯
'calendar':0, # 日历
'tape':0,     # tape, warmers
              # 胶带, 暖宝宝
'rainy':0,    # 下雨的
'snow':0,     # 下雪的
'insect':0,   # 昆虫
'zippo':0,    # 打火机
'fish-pole':0,  # 鱼竿
'animal':0,     # 动物
'boat':0,       # 船
'sun':0,        # 
'ticket':0,     # (电影)票, (车)票
'knife':0,      # 刀

'string':0,     # string, rope, chain
                # 绳, 锁链
'trap':0,       # 陷阱
'stage':0,      # 舞台
'crowd':0,      # 人群
'key':0,        # 钥匙
'price':0,      # 价格
'guitar':0,     # 吉他(should be replced with 'instrument'?)
'sculpture':0,  # 雕塑
'vase':0,       # 花瓶
'extinguisher':0, #灭火器
'wedding-dress':0, # 婚礼服，婚纱
'monster':0,    # 鬼怪
'dispenser':0,  # vending machine
                # 自动售货机
'curve':0,      # 一团线
'flash':0,      # 闪电
'bleed':0,      # 血  (to be replaced with 'blood'? )
'subway':0,     # 地铁，地下通道
'baby':0,       # infant 
                # 婴儿
'wet':0,        # 淋湿
'thermometer':0, # 温度计
'noise':0,      # 噪音
'mark':0,       # 记号
'cosplay':0,    # 天使，恶魔，僵尸，
                # angel, ghost, zombie，cosplay
'balloon':0,    # 气球  
'broken':0,     # something broken 
                # 破碎的东西
'sth-open':0,   # something is open
                # 被打开的某物
'sign':0,     # Hojo Tsukasa's sign, shop's logo
              # 北条司的签名, 商店的logo
'logo':0,     # shop logo. (to be replaced by 'sign'?)
              # 商店的logo
'p-slipper':0, # slipper as a property
'p-shoe':0,    # shoe as a property
'injury':0,    # 伤痕(to be replaced with 'scar'?)
'smoking-set':0,  # smoking set
                  # 烟, 烟灰缸, 打火机, 烟盒
'wig':0,  # 假发 
}# end of prop
# ------------------------------------------------------------------------------
# EFFECT
# ------------------------------------------------------------------------------
effect = {
'fx-x':0,     # For the effect which I don't know how to describe
              # 标记暂时不知如何表述的效果
'focus':0,    # For the effect that something is focused
              # 聚焦的效果
'wind':0,     # e.g. hair is blew by the wind
              # 例如头发被风吹拂的效果
'fast':0,     # For the effect that something is moving fast
              # 快速移动的效果
'clash':0,    # For being surprised, or clash
              # 表示惊讶，或碰撞
'mosaic':0,   # 马赛克
'sunny':0,    # Sun light
              # 阳光明媚的效果
'gloomy':0,   # 阴天，阴郁的
'hot':0,      # sun-baked, blazing, burning sun, scorching sun, 
              # 烈日炎炎的
'dialogue':0, # 对话框
'text':0,     # 文字
#'black':0,    # black background
#              # 黑色背景
'icon':0,     # 图标
'heart':0,    # icon heart
              # 心形图标
'line':0,     # line effect
              # 线条组成的效果
'dark':0,     # dark effect
              # 暗色效果
'tension':0,  # 紧张
'fire':0,     # fire effect
              # 火焰特效
'annotation':0, # 注释
'question':0,   # question icon
                # 问号
'music':0,      # 音乐图标
'arrow':0,      # 箭头图标
'blur':0,       # 模糊
'dof':0,        # depth of field
                # 景深
'glow':0,       # 光晕
'gradient':0,   # gradient effect
                # (梯度)渐变效果
'distort':0,    # lens distortion
                # 镜头畸变/弯曲
'death':0,      # death icon
'dapple':0,     # 光斑
'luminescent':0, # 荧光 
'glitter':0,    # 闪烁，波光粼粼
'white-hair':0,  # to be removed
'yarn':0,        # a ball of string
}# end of effect

